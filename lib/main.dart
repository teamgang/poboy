import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'package:poboy/app.dart';
import 'package:poboy/error_app.dart';
import 'package:poboy/utils/boi_channels.dart';
import 'package:pushboy_lib/pushboy_lib.dart';

void main() async {
  King king;
  try {
    WidgetsFlutterBinding.ensureInitialized();
    final pushService = PushService(debug: true);
    pushService.initialize();

    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform);
    FirebaseMessaging.onBackgroundMessage(_firebaseHandler);
    FirebaseMessaging.onMessage.listen(_firebaseHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(_firebaseHandler);

    //NOTE: the following enables foreground notifications on iOS
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );

    FirebaseAnalytics.instance;

    king = await makeDefaultKing();
    runApp(MyApp(king: king));
  } catch (e) {
    runApp(ErrorApp());
  }
}

Future<void> _firebaseHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as
  // Firestore, make sure you call `initializeApp` before using other Firebase
  // services.
  await Firebase.initializeApp();

  print('Handling a firebase message: ${message.messageId}');

  final boiTx = BoiTx();
  await boiTx.unpackFromMessage(message.data);

  //NOTE: to sync settings, we need to call awesomeNotifications to get context
  await AwesomeNotifications().createNotification(
    content: await boiTx.asNotification(),
  );
}
