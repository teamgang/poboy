import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkSubSettingsPage extends StatefulWidget {
  const NetworkSubSettingsPage({required this.networkSub});
  final NetworkSub networkSub;

  @override // refresh to make for UX
  NetworkSubSettingsPageState createState() => NetworkSubSettingsPageState();
}

class NetworkSubSettingsPageState extends State<NetworkSubSettingsPage> {
  late final NetworkSub _copy;
  String _failureMsg = '';
  bool _hasAnyValueChanged = false;
  bool _requestInProgress = false;

  @override
  initState() {
    super.initState();
    _sub.syncValues();
    final king = King.of(context);
    _copy = king.dad.networkSubProxy
        .getNetworkSubRef(networkSubId: king.conf.zerosUuid);
    _copy.loadFromAlt(_sub);
  }

  NetworkSub get _sub {
    return widget.networkSub;
  }

  bool _canSync(Todd todd) {
    return todd.isSignedIn && _sub.isLinkedToSurfer;
  }

  _toggleIsLinkedToSurfer() {
    _copy.toggleIsLinkedToSurfer();
    _checkForValueChanges();
  }

  _toggleSilenced() {
    _copy.isSilenced = !_copy.isSilenced;
    _checkForValueChanges();
  }

  _toggleSynced() {
    _copy.syncEnabled = !_copy.syncEnabled;
    _checkForValueChanges();
  }

  _checkForValueChanges() {
    setState(() {
      _hasAnyValueChanged = !_sub.deepEquals(_copy);
    });
  }

  @override
  Widget build(BuildContext context) {
    final todd = King.of(context).todd;

    return MobileScaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(8, 24, 8, 24),
        child: Observer(
          builder: (_) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: ElevatedButton(
                  onPressed: _hasAnyValueChanged
                      ? () {
                          _saveChanges(context);
                        }
                      : null,
                  child: const Text18('Save changes'),
                ),
              ),
              FailureMsg(_failureMsg),
              //

              todd.isSignedIn
                  ? const SizedBox.shrink()
                  : const Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Text18(
                          'You must be signed in to link this subscription to your account and to sync notification settings across devices.',
                          maxLines: 6,
                          style: TextStyle(
                            color: Colors.grey,
                            fontStyle: FontStyle.italic,
                          )),
                    ),

              todd.isSignedIn
                  ? Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      child: Observer(
                        builder: (_) => Row(
                          children: <Widget>[
                            Checkbox(
                              value: _copy.isLinkedToSurfer,
                              onChanged: (bool? newValue) {
                                _toggleIsLinkedToSurfer();
                              },
                            ),
                            const Expanded(
                              child: Text18('Link subscription to your account',
                                  maxLines: 4),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const SizedBox.shrink(),

              _canSync(todd)
                  ? Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                            value: _copy.syncEnabled,
                            onChanged: todd.isSignedIn
                                ? (bool? newValue) {
                                    _toggleSynced();
                                  }
                                : null,
                          ),
                          const Expanded(
                            child: Text18(
                                'Sync these settings with my other devices',
                                maxLines: 4),
                          ),
                        ],
                      ),
                    )
                  : const SizedBox.shrink(),
              //

              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: _copy.isSilenced,
                      onChanged: (bool? newValue) {
                        _toggleSilenced();
                      },
                    ),
                    const Text18('Silence this network'),
                  ],
                ),
              ),

              Center(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: BaIconButton(
                    iconData: Icons.cancel,
                    text: 'Unsubscribe',
                    onTap: () {
                      _showConfirmUnsubscribeModal(context);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showConfirmUnsubscribeModal(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return SizedBox(
          height: 200,
          width: 400,
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                        'Are you sure you want to unsubscribe from the network "${_sub.network.name}"?'),
                  ),
                  const SizedBox(height: 12),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                            child: const Text('Yes'),
                            onPressed: () {
                              _onUnsubscribe(context);
                            }),
                        const SizedBox(width: 60),
                        ElevatedButton(
                          child: const Text('No'),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ]),
                ]),
          ),
        );
      },
    );
  }

  _onUnsubscribe(BuildContext context) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      var king = King.of(context);

      ApiResponse ares = await king.lip.api(
        EndpointsV1.networkSubUnsubscribe,
        payload: {
          'network_sub_id': _sub.networkSubId,
        },
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        king.dad.networkSubProxy.getAndLoadNetworkSubIdsForInstanceId();
        if (!mounted) return;
        Navigator.of(context).pushReplacementNamed(Routes.home);
      } else {
        setState(() {
          _failureMsg = 'Failed to unsubscribe';
        });
      }
    }
  }

  _saveChanges(BuildContext context) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      _sub.loadFromAlt(_copy);
      _sub.saveLocal();
      var ares = await _sub.saveToRemote();

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        _sub.loadFromApi(); // refresh to make for UX
        if (!mounted) return;
        Navigator.of(context).pop();
      } else {
        setState(() {
          _failureMsg = 'Failed to save changes';
        });
      }
    }
  }
}
