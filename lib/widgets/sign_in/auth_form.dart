import 'package:flutter/material.dart';
import 'package:auth_buttons/auth_buttons.dart';

import 'package:poboy/widgets/sign_in/sign_in_page.dart';
import 'package:pushboy_lib/pushboy_lib.dart';

class AuthFormFields {
  String email = '';
  String password = '';
}

class AuthForm extends StatefulWidget {
  const AuthForm(this.whichForm);
  final AuthPages whichForm;

  @override
  AuthFormState createState() => AuthFormState();
}

class AuthFormState extends State<AuthForm> {
  String _failureMsg = '';
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  bool _submitFailed = false;
  AuthFormFields fields = AuthFormFields();

  String validateEmail(String email) {
    if (email != '') {
      if (email.length < 5) {
        return 'Email is too short';
      }
      int atPosition = email.indexOf('@');
      int dotPosition = email.lastIndexOf('.');
      if (dotPosition == -1 || atPosition == -1 || dotPosition <= atPosition) {
        return 'Email format is invalid';
      }
    }
    if (email == '') {
      return 'You must provide an email.';
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    String banner = '';
    switch (widget.whichForm) {
      case AuthPages.register:
        banner = 'Create your account.';
        break;
      case AuthPages.signIn:
        banner = 'Sign in to your account.';
        break;
    }

    final theme = King.of(context).theme;

    return Container(
      width: 400,
      color: Colors.grey[150],
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              const SizedBox(height: 20),
              Text(banner, style: const TextStyle(fontSize: 16)),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: const Key(XKeys.authFormEmailField),
                decoration: const InputDecoration(
                  labelText: 'Email',
                ),
                maxLength: 320,
                autocorrect: false,
                onChanged: (value) {
                  if (this._submitFailed) {
                    setState(() {
                      this._submitFailed = false;
                      _failureMsg = '';
                    });
                  }
                  fields.email = value.trim();
                },
                validator: (value) {
                  String? error = validateEmail(value ?? '');
                  return error == '' ? null : error;
                },
              ),
              const SizedBox(height: 14),

              BasedCreatePassword(
                textFieldKey: const Key(XKeys.authFormPasswordField),
                onChanged: (password) {
                  if (this._submitFailed) {
                    setState(() {
                      this._submitFailed = false;
                      _failureMsg = '';
                    });
                  }
                  fields.password = password;
                },
              ),
              const SizedBox(height: 32),
              //

              _isLoading
                  ? const Center(child: CircularProgressIndicator())
                  : ElevatedButton(
                      key: const Key(XKeys.authFormRegisterOrSignInButton),
                      onPressed: () {
                        FocusManager.instance.primaryFocus?.unfocus();
                        _onSubmit(context);
                      },
                      child: widget.whichForm == AuthPages.register
                          ? const Text('Register')
                          : const Text('Sign in'),
                    ),
              const SizedBox(height: 32),

              widget.whichForm == AuthPages.register
                  ? const SizedBox.shrink()
                  : TextButton(
                      key: const Key(XKeys.authFormResetPasswordButton),
                      onPressed: () => Navigator.of(context).pushNamed(
                          Routes.loggedOutSurferChangePasswordRequest),
                      child: const Text('Forgot password?',
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 14,
                            fontStyle: FontStyle.italic,
                          )),
                    ),
              const SizedBox(height: 24),

              FailureMsg(_failureMsg),
              const SizedBox(height: 24),

              AppleAuthButton(
                darkMode: theme.isDark,
                onPressed: () => _onAppleSignIn(context),
              ),
              const SizedBox(height: 24),

              GoogleAuthButton(
                darkMode: theme.isDark,
                onPressed: () => _onGoogleSignIn(context),
              ),
              const SizedBox(height: 24),

              //SignInButton(
              //Buttons.Apple,
              //onPressed: King.of(context).todd.signInWithApple(),
              //),
            ],
          ),
        ),
      ),
    );
  }

  void _onAppleSignIn(BuildContext context) async {
    var todd = King.of(context).todd;
    setState(() {
      _isLoading = true;
    });

    var ares = await todd.signInWithApple();

    if (ares.isOk) {
      if (!mounted) return;
      Navigator.of(context).pushNamedAndRemoveUntil(
          Routes.home, (Route<dynamic> route) => false);
    } else {
      setState(() {
        _failureMsg = ares.peekErrorMsg(defaultText: 'Could not sign in.');
        this._submitFailed = true;
      });
    }

    setState(() {
      _isLoading = false;
    });
  }

  void _onGoogleSignIn(BuildContext context) async {
    var todd = King.of(context).todd;
    setState(() {
      _isLoading = true;
    });

    var ares = await todd.signInWithGoogle();

    if (ares.isOk) {
      if (!mounted) return;
      Navigator.of(context).pushNamedAndRemoveUntil(
          Routes.home, (Route<dynamic> route) => false);
    } else {
      setState(() {
        _failureMsg = ares.peekErrorMsg(defaultText: 'Could not sign in.');
        this._submitFailed = true;
      });
    }

    setState(() {
      _isLoading = false;
    });
  }

  void _onSubmit(BuildContext context) async {
    var todd = King.of(context).todd;
    setState(() {
      _isLoading = true;
    });

    if (_formKey.currentState?.validate() ?? false) {
      switch (widget.whichForm) {
        case AuthPages.register:
          var ares = await todd.registerWithEmail(
              email: fields.email, password: fields.password);

          if (ares.isOk) {
            if (!mounted) return;
            Navigator.of(context).pushReplacementNamed(Routes.confirmEmail);
          } else {
            setState(() {
              _failureMsg =
                  ares.peekErrorMsg(defaultText: 'Registration failed.');
              this._submitFailed = true;
            });
          }
          break;

        case AuthPages.signIn:
          var ares = await todd.signInWithEmail(
              email: fields.email, password: fields.password);

          if (ares.isOk) {
            if (!mounted) return;
            Navigator.of(context).pushNamedAndRemoveUntil(
                Routes.home, (Route<dynamic> route) => false);
          } else {
            setState(() {
              _failureMsg =
                  ares.peekErrorMsg(defaultText: 'Could not sign in.');
              this._submitFailed = true;
            });
          }
          break;
      }
    }

    setState(() {
      _isLoading = false;
    });
  }
}
