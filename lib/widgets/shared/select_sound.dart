import 'package:flutter/material.dart';
import 'package:assets_audio_player/assets_audio_player.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class SelectSoundPage extends StatelessWidget {
  const SelectSoundPage();

  @override
  Widget build(BuildContext context) {
    var player = AssetsAudioPlayer.newPlayer();

    return MobileScaffold(
        body: ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: SoundResource.values.length,
      itemBuilder: (BuildContext context, int index) {
        final res = SoundResource.values[index];

        return Row(children: <Widget>[
          InkWell(
            onTap: () async {
              await player.open(
                Audio(res.assetPath),
                autoStart: true,
              );
            },
            child: const Padding(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Icon(Icons.play_arrow),
            ),
          ),
          //

          InkWell(
            onTap: () {
              Navigator.of(context).pop(res);
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: Text20(res.displayName),
            ),
          ),
        ]);
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    ));
  }
}
