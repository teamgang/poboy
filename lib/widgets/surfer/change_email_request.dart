import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class SurferChangeEmailRequestPage extends StatefulWidget {
  const SurferChangeEmailRequestPage({required this.surfer});
  final Surfer surfer;

  @override
  _SurferChangeEmailRequestPageState createState() =>
      _SurferChangeEmailRequestPageState();
}

class _SurferChangeEmailRequestPageState
    extends State<SurferChangeEmailRequestPage> {
  String _msg = '';
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return MobileScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //
              const Text18(
                  'To initiate an email change, click the link below. You will be emailed a link which will allow you to change your email.',
                  maxLines: 6),
              const SizedBox(height: 12),

              const Text16(
                  'Warning: Changing your email will unlink this account from associated Apple or Google logins.'),
              const SizedBox(height: 12),

              FunctionCard(
                text: 'Change my email',
                onTap: () {
                  _onSubmit(context);
                },
              ),
              const SizedBox(height: 12),

              FailureMsg(_msg),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      this.setState(() {
        _msg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.surferChangeEmailRequest,
        payload: {
          'surfer_id': widget.surfer.surferId,
        },
      );

      this.setState(() {
        _requestInProgress = false;
        this.setState(() {
          _msg = 'Email sent.';
        });
      });
      if (ares.isOk) {
      } else {
        this.setState(() {
          _msg = 'Failed to submit change.';
        });
      }
    }
  }
}
