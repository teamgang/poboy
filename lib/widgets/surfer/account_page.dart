import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class AccountPage extends StatefulWidget {
  const AccountPage();

  @override
  AccountPageState createState() => AccountPageState();
}

class AccountPageState extends State<AccountPage> {
  final surfer = Surfer();

  @override
  initState() {
    super.initState();
    final king = King.of(context);
    surfer.loadFromApi(context, king.todd.surferId);
  }

  @override
  Widget build(BuildContext context) {
    return MobileScaffold(
      body: Scrollbar(
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                AccountCluster(surfer: surfer),
                //

                OtherInformationCluster(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AccountCluster extends StatelessWidget {
  const AccountCluster({required this.surfer});
  final Surfer surfer;

  @override
  Widget build(BuildContext context) {
    final todd = King.of(context).todd;

    if (todd.isSignedIn) {
      return OptionCluster(
        title: 'Account',
        children: <Widget>[
          OptionRow(
              option: 'Email',
              value: surfer.email,
              onTap: () {
                Navigator.of(context).pushNamed(Routes.surferChangeEmailRequest,
                    arguments: Args(surfer: surfer));
              }),
          const Divider(color: Colors.black, height: 0),
          //

          OptionRow(
              option: 'Password',
              value: '********',
              onTap: () {
                Navigator.of(context).pushNamed(
                    Routes.surferChangePasswordRequest,
                    arguments: Args(surfer: surfer));
              }),
          const Divider(color: Colors.black, height: 0),

          OptionRowStatic(
              option: 'Sign Out',
              onTap: () {
                todd.signOut();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    Routes.home, (Route<dynamic> route) => false);
              }),
          const Divider(color: Colors.black, height: 0),

          OptionRowStatic(
              option: 'Delete Account',
              onTap: () {
                Navigator.of(context).pushNamed(
                    Routes.surferConfirmDeleteAccount,
                    arguments: Args(surfer: surfer));
              }),
        ],
      );
    } else {
      return OptionCluster(
        title: 'Account',
        children: <Widget>[
          OptionRowStatic(
              option: 'Sign In',
              onTap: () {
                Navigator.of(context).pushNamed(Routes.signIn);
              }),
          const Divider(color: Colors.black, height: 0),
        ],
      );
    }
  }
}

class OtherInformationCluster extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String id = 'no id';
    String token = King.of(context).conf.firebaseToken;
    if (token.length > 8) {
      id = token.substring(0, 8);
    }
    return OptionCluster(
      title: 'Other Information',
      children: <Widget>[
        OptionRowStatic(
            option: 'Privacy Policy',
            onTap: () {
              Navigator.of(context).pushNamed(Routes.surferPrivacy);
            }),
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
          child: Text18('Device ID:  $id'),
        ),
      ],
    );
  }
}
