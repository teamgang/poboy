import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class SurferConfirmDeleteAccountPage extends StatefulWidget {
  const SurferConfirmDeleteAccountPage({required this.surfer});
  final Surfer surfer;

  @override
  SurferConfirmDeleteAccountPageState createState() =>
      SurferConfirmDeleteAccountPageState();
}

class SurferConfirmDeleteAccountPageState
    extends State<SurferConfirmDeleteAccountPage> {
  String _msg = '';
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return MobileScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //
              const Text18(
                  'Are you sure you want to delete your account? A deleted account cannot be recovered.',
                  maxLines: 6),
              const SizedBox(height: 12),

              FunctionCard(
                text: 'Yes',
                onTap: () {
                  _onSubmit(context);
                },
              ),
              const SizedBox(height: 12),

              FailureMsg(_msg),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      final king = King.of(context);
      this.setState(() {
        _msg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await king.lip.api(
        EndpointsV1.surferDelete,
        payload: {
          'surfer_id': widget.surfer.surferId,
        },
      );

      this.setState(() {
        this.setState(() {
          _msg = 'Account deleted';
        });
      });

      await Future.delayed(
        const Duration(seconds: 3),
        () => {},
      );

      this.setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        king.todd.signOut();
        if (!mounted) return;
        Navigator.of(context).pushNamedAndRemoveUntil(
            Routes.home, (Route<dynamic> route) => false);
      } else {
        this.setState(() {
          _msg = 'Failed to delete account';
        });
      }
    }
  }
}
