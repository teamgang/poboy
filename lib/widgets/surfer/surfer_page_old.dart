import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class SurferPage extends StatefulWidget {
  const SurferPage();

  @override
  _SurferPageState createState() => _SurferPageState();
}

class _SurferPageState extends State<SurferPage> {
  final surfer = Surfer();

  @override
  initState() {
    super.initState();
    final king = King.of(context);
    surfer.loadFromApi(context, king.todd.surferId);
  }

  @override
  Widget build(BuildContext context) {
    return MobileScaffold(
      body: Scrollbar(
        isAlwaysShown: true,
        child: SingleChildScrollView(
          child: Center(
            child: Observer(
              builder: (_) => Padding(
                padding: const EdgeInsets.all(24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        const Text18('Email'),
                        const SizedBox(width: 8),
                        TextButton(
                          child: const BaText('Edit'),
                          onPressed: () {
                            Navigator.of(context).pushNamed(
                                Routes.surferChangeEmailRequest,
                                arguments: Args(surfer: surfer));
                          },
                        ),
                      ],
                    ),
                    Text18(surfer.email),
                    const BaDividerOverview(),
                    //

                    Row(
                      children: <Widget>[
                        const Text18('Password'),
                        const SizedBox(width: 8),
                        TextButton(
                          child: const BaText('Edit'),
                          onPressed: () {
                            Navigator.of(context).pushNamed(
                                Routes.surferChangePasswordRequest,
                                arguments: Args(surfer: surfer));
                          },
                        ),
                      ],
                    ),
                    const BaDividerOverview(),

                    Center(
                      child: FunctionCard(
                          text: 'Sign Out',
                          onTap: () {
                            King.of(context).todd.signOut();
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                Routes.home, (Route<dynamic> route) => false);
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
