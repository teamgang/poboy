import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class FlagBoiPage extends StatefulWidget {
  const FlagBoiPage({required this.boiId});
  final String boiId;

  @override
  _FlagBoiPageState createState() => _FlagBoiPageState();
}

class _FlagBoiPageState extends State<FlagBoiPage> {
  String _category = '';
  String _failureMsg = '';
  String _msg = '';
  final int _maxLength = 500;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return MobileSliverScaffold(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      slivers: <Widget>[
        const SizedBox(height: 24),
        const Text20('Flag Content'),
        const SizedBox(height: 24),
        const Text('Category of offense:'),

        Row(children: <Widget>[
          Radio<String>(
            value: 'illegal_porn',
            groupValue: _category,
            onChanged: (String? value) {
              _category = value ?? '';
            },
          ),
          const TextCard('Illegal (porn)'),
        ]),

        Row(children: <Widget>[
          Radio<String>(
            value: 'illegal_drugs',
            groupValue: _category,
            onChanged: (String? value) {
              _category = value ?? '';
            },
          ),
          const TextCard('Illegal (drugs)'),
        ]),

        Row(children: <Widget>[
          Radio<String>(
            value: 'abusive',
            groupValue: _category,
            onChanged: (String? value) {
              _category = value ?? '';
            },
          ),
          const TextCard('Abusive'),
        ]),

        const SizedBox(height: 20),

        const Text('Give more information (optional)'),
        TextFormField(
          enabled: !_requestInProgress,
          keyboardType: TextInputType.multiline,
          maxLength: _maxLength,
          style: Theme.of(context).textTheme.headline6,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            counterText: '${_msg.length}/$_maxLength',
          ),
          onChanged: (newValue) {
            _msg = newValue;
          },
        ),
        const SizedBox(height: 20),
        //

        TextButton(
          child: const Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: const Text('Submit'),
          onPressed: () {
            _submitFlag();
          },
        ),

        _failureMsg.isEmpty
            ? const SizedBox.shrink()
            : Text(_failureMsg, style: failureStyle()),
      ],
    );
  }

  _submitFlag() async {
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.flagBoi,
        payload: {
          'boi_id': widget.boiId,
          'instance_id': King.of(context).conf.firebaseToken,
          'category': _category,
          'msg': _msg,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit report.';
        });
      }
    }
  }
}
