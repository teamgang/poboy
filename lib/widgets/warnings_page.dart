import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class WarningsPage extends StatefulWidget {
  @override
  WarningsPageState createState() => WarningsPageState();
}

class WarningsPageState extends State<WarningsPage> {
  bool _isAwesomeNotificationsAllowed = false;

  @override
  initState() {
    super.initState();
    _setIsAllowedIfAllowed();
  }

  _setIsAllowedIfAllowed() {
    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      setState(() {
        _isAwesomeNotificationsAllowed = isAllowed;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _setIsAllowedIfAllowed();
    return MobileScaffold(
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Observer(
          builder: (_) => Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: _isAwesomeNotificationsAllowed
                      ? const Text18('No warnings')
                      : Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            const Text18(
                                'Hey! Your settings prevent "Push"-Boi from sending you "PUSH" notifications! PushBoi needs the Push notifications to do right.',
                                maxLines: 4),
                            const SizedBox(height: 20),
                            //

                            ElevatedButton(
                              onPressed: () {
                                AwesomeNotifications()
                                    .isNotificationAllowed()
                                    .then(
                                  (isAllowed) {
                                    if (!isAllowed) {
                                      showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title:
                                              const Text('Allow Notifications'),
                                          content: const Text(
                                              'Our app would like to send you notifications'),
                                          actions: [
                                            TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: const Text(
                                                'Don\'t Allow',
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 18),
                                              ),
                                            ),
                                            //

                                            TextButton(
                                              onPressed: () async {
                                                AwesomeNotifications()
                                                    .requestPermissionToSendNotifications()
                                                    .then((_) {
                                                  Navigator.pop(context);
                                                });
                                              },
                                              child: const Text(
                                                'Allow',
                                                style: TextStyle(
                                                  color: Colors.teal,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }
                                  },
                                );
                              },
                              child: const Padding(
                                padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                                child: Center(
                                  child: Text(
                                    'Click here to enable PUSH notifications',
                                    maxLines: 3,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
