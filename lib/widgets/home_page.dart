import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sliver_glue/mobx.dart';

import 'package:poboy/widgets/welcome.dart';
import 'package:pushboy_lib/pushboy_lib.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  initState() {
    super.initState();

    FlutterAppBadger.updateBadgeCount(0);
    AwesomeNotifications().isNotificationAllowed().then(
      (isAllowed) {
        if (!isAllowed) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Allow Notifications'),
              content:
                  const Text('Our app would like to send you notifications'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text(
                    'Don\'t Allow',
                    style: TextStyle(color: Colors.grey, fontSize: 18),
                  ),
                ),
                //

                TextButton(
                  onPressed: () => AwesomeNotifications()
                      .requestPermissionToSendNotifications()
                      .then((_) => Navigator.pop(context)),
                  child: const Text(
                    'Allow',
                    style: TextStyle(
                      color: Colors.teal,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final king = King.of(context);
    final networkSubIds = king.dad.networkSubProxy.getNetworkSubIdsRef();
    king.dad.networkSubProxy.getAndLoadNetworkSubIdsForInstanceId();

    return MobileSliverScaffold(
      slivers: <Widget>[
        SliverToBoxAdapter(
          child: Observer(builder: (_) {
            final networkSubIdsFresh =
                king.dad.networkSubProxy.getNetworkSubIdsRef();
            return networkSubIdsFresh.isEmpty
                ? Welcome()
                : const SizedBox.shrink();
          }),
        ),
        SliverGlueObservableList(
          data: networkSubIds,
          builder: (context, _, index, isFirst, isLast) {
            return Observer(builder: (_) {
              final networkSub = king.dad.networkSubProxy
                  .getNetworkSubRef(networkSubId: networkSubIds[index]);

              return networkSub.networkSubId.isNotEmpty
                  ? NetworkSubSlab(
                      index: index,
                      networkSub: networkSub,
                    )
                  : const SizedBox.shrink();
            });
          },
        ),
      ],
    );
  }
}
