import 'package:flutter/material.dart';

import 'package:poboy/utils/boi_test.dart';
import 'package:pushboy_lib/pushboy_lib.dart';

class HelpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MobileScaffold(
      body: Column(
        children: <Widget>[
          const Text16(
              'Go to https://pushboi.io to create an account and send your own notifications.',
              maxLines: 6),
          ElevatedButton(
            child: const Text(
                'If this link does not send you a notification, please contact us.'),
            onPressed: () {
              createTestNotification(SoundResource.manStretching);
            },
          ),
        ],
      ),
    );
  }
}
