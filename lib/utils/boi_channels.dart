import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class PushService {
  PushService({required this.debug});
  bool debug;

  void initialize() {
    final List<NotificationChannel> channels = [];
    for (final item in SoundResource.values) {
      //var res = soundResourceFromString(item)

      print('creating channle ${item.name}');
      if (item == SoundResource.none) {
        channels.add(NotificationChannel(
          channelGroupKey: 'basic_channel_group',
          channelKey: item.name,
          channelName: 'Basic notifications',
          channelDescription: 'Notifications without sound',
          channelShowBadge: true,
          defaultColor: const Color(0xFF006400),
          ledColor: Colors.green,
          //

          playSound: false,
          importance: NotificationImportance.Max,
        ));
      } else {
        channels.add(NotificationChannel(
          channelGroupKey: 'basic_channel_group',
          channelKey: item.name,
          channelName: 'Basic notifications',
          channelDescription: 'Notifications with sound',
          channelShowBadge: true,
          defaultColor: const Color(0xFF006400),
          ledColor: Colors.green,
          //

          playSound: true,
          soundSource: item.path,
          importance: NotificationImportance.Max,
        ));
      }
    }

    channels.add(NotificationChannel(
      channelGroupKey: 'command_group',
      channelKey: 'command',
      channelName: 'Commands',
      channelDescription: 'For commands',
      channelShowBadge: false,
      enableLights: false,
      enableVibration: false,
      playSound: false,
      importance: NotificationImportance.None,
    ));

    channels.add(NotificationChannel(
      channelGroupKey: 'silent',
      channelKey: 'silent',
      channelName: 'silent',
      channelDescription: 'For silenced notifications',
      channelShowBadge: false,
      enableLights: false,
      enableVibration: false,
      playSound: false,
      importance: NotificationImportance.None,
    ));

    //NOTE: any changes to notification channels requires reinstallation
    AwesomeNotifications().initialize(
      // set the icon to null if you want to use the default app icon
      'resource://drawable/res_noti',
      //null,
      channels,

      // Channel groups are only visual and are not required
      channelGroups: [
        NotificationChannelGroup(
          channelGroupkey: 'basic_channel_group',
          channelGroupName: 'Basic group',
        )
      ],
      debug: debug,
    );
  }
}
