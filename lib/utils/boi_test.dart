import 'package:awesome_notifications/awesome_notifications.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

int createBoiId() {
  return (DateTime.now().millisecondsSinceEpoch / 1000).floor();
}

Future<void> createTestNotification(SoundResource res) async {
  await AwesomeNotifications().createNotification(
    content: NotificationContent(
      id: createBoiId(),
      channelKey: res.name,
      title: '<b>generated in app title<b>',
      body: '<br /><br />generated in app body',
      bigPicture: 'https://i.imgur.com/1M3cRXh.png',
      //icon: 'https://i.imgur.com/hjDWHuI.jpeg',
      largeIcon: 'https://i.imgur.com/hjDWHuI.jpeg',
      //notificationLayout: NotificationLayout.BigPicture,
      notificationLayout: NotificationLayout.Default,
    ),
  );
}
