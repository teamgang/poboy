import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

enum MetricType {
  boiCreated,
  boiDisplayed,
  boiTapped,
  boiDismissed,
  boiViewedInApp,
}

extension ConversionExtenstions on MetricType {
  String get reportName {
    switch (this) {
      case MetricType.boiCreated:
        return 'boiCreated';
      case MetricType.boiDisplayed:
        return 'boiDisplayed';
      case MetricType.boiTapped:
        return 'boiTapped';
      case MetricType.boiDismissed:
        return 'boiDismissed';
      case MetricType.boiViewedInApp:
        return 'boiViewedInApp';
    }
  }
}

class MetricReporter {
  static saveToReportLater(MetricType metricType, String boiId) async {
    final box = await Hive.openBox(metricType.reportName);
    List<String> bois = await box.get('bois') ?? [];
    bois.add(boiId);
    await box.put('bois', bois);
  }

  static tryToReportAllFailedBoiMetrics({
    required BuildContext context,
  }) async {
    for (final metricType in MetricType.values) {
      tryToReportAllFailedBoisForMetricType(
        context: context,
        metricType: metricType,
      );
    }
  }

  static tryToReportAllFailedBoisForMetricType({
    required BuildContext context,
    required MetricType metricType,
  }) async {
    final king = King.of(context);
    final box = await Hive.openBox(metricType.reportName);
    List<String> failedIds = await box.get('failedIds') ?? [];
    List<String> failedAgainIds = [];

    for (final failedId in failedIds) {
      ApiResponse ares = await king.lip.api(
        EndpointsV1.boiMetricReport,
        payload: {
          'boi_id': failedId,
          'instance_id': king.conf.firebaseToken,
          'report_name': metricType.reportName,
        },
      );

      if (ares.isNotOk) {
        failedAgainIds.add(failedId);
      }
    }

    if (failedIds.length != failedAgainIds.length) {
      //NOTE: if these are the same length, then nothing changed
      await box.put('failedIds', failedAgainIds);
    }
  }

  static tryToReportBoiMetric({
    required String boiId,
    required BuildContext context,
    required MetricType metricType,
  }) async {
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.boiMetricReport,
      payload: {
        'boi_id': boiId,
        'instance_id': King.of(context).conf.firebaseToken,
        'report_name': metricType.reportName,
      },
    );

    if (ares.isNotOk) {
      final box = await Hive.openBox(metricType.reportName);
      List<String> failedIds = await box.get('failedIds') ?? [];
      failedIds.add(boiId);
      await box.put('failedIds', failedIds);
    }
  }
}
