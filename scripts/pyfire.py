#!/usr/bin/env python

import json
import requests


device_token = 'dVfNLHxtT_6feFQY39pZU8:APA91bHtV566a6BOPjkRiBzaDEouFSSSVd4PMmmnZqqSPljiaLMXUmEEUcW7I4hBsIwJcJ9hrpJtxbxsEyGg-JkO8eMvLRUfTnDv4-MisK5Va6um1pgHke0pGQ9ofUqpCpAXUcsrAPtO'
server_token = 'AAAACO2c0WA:APA91bFT88dFZ-7qKlB--dRoeSbRPrba8v4qOWY1zdR7TBFbm218HW3ztYmx    -LheblZ-gFJL3WeTFXkoRTUbHt9h0uehpfABI7qv_9FWcxjjbkT6-CcNgF6HY5d4yIhihNagjQBmQU_t'

headers = {
    'Content-Type': 'application/json',
    'Authorization': f'key=$server_token',
}

#NOTE: this is legacy method
data = {
    "direct_boot_ok": True,  # required for legacy
    "to" : device_token,
    "mutable_content" : True,
    "content_available": True,
    "priority": "high",
    "data" : {
        "content": {
            "id": 100,
            "channelKey": "basic_channel",
            "title": "Huston!\nThe eagle has landed!",
            "body": "A small step for a man, but a giant leap to Flutter's community!",
            "notificationLayout": "BigPicture",
            "largeIcon": "https://media.fstatic.com/kdNpUx4VBicwDuRBnhBrNmVsaKU=/full-fit-in/290x478/media/artists/avatar/2013/08/neil-i-armstrong_a39978.jpeg",
            "bigPicture": "https://www.dw.com/image/49519617_303.jpg",
            "showWhen": True,
            "autoDismissable": True,
            "privacy": "Private"
        },
        "actionButtons": [
            {
                "key": "REPLY",
                "label": "Reply",
                "autoDismissable": True,
                "buttonType":  "InputField"
            },
            {
                "key": "ARCHIVE",
                "label": "Archive",
                "autoDismissable": True
            }
        ],
        "schedule": {
            "timeZone": "America/New_York",
            "hour": "10",
            "minute": "0",
            "second": "0",
            "millisecond": "0",
            "allowWhileIdle": True,
            "repeat": True
        }
    }
}

resp = requests.post(
    'https://fcm.googleapis.com/fcm/send',
    headers=headers,
    data=json.dumps(data)
)

print(resp.status_code)
if resp.status_code != 200:
    raise RuntimeError('ERROR: request failed')
else:
    print(resp.json())
