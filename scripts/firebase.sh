#!/bin/bash

curl --header "Content-Type: application/json" \
	--request POST \
	--data cat << EndOfMessage
{
    "to" : "AAAACO2c0WA:APA91bFT88dFZ-7qKlB--dRoeSbRPrba8v4qOWY1zdR7TBFbm218HW3ztYmx-LheblZ-gFJL3WeTFXkoRTUbHt9h0uehpfABI7qv_9FWcxjjbkT6-CcNgF6HY5d4yIhihNagjQBmQU_t",
    "mutable_content" : true,
    "content_available": true,
    "priority": "high",
    "data" : {
        "content": {
            "id": 100,
            "channelKey": "basic_channel",
            "title": "Huston!\nThe eagle has landed!",
            "body": "A small step for a man, but a giant leap to Flutter's community!",
            "notificationLayout": "BigPicture",
            "largeIcon": "https://media.fstatic.com/kdNpUx4VBicwDuRBnhBrNmVsaKU=/full-fit-in/290x478/media/artists/avatar/2013/08/neil-i-armstrong_a39978.jpeg",
            "bigPicture": "https://www.dw.com/image/49519617_303.jpg",
            "showWhen": true,
            "autoDismissable": true,
            "privacy": "Private"
        },
        "actionButtons": [
            {
                "key": "REPLY",
                "label": "Reply",
                "autoDismissable": true,
                "buttonType":  "InputField"
            },
            {
                "key": "ARCHIVE",
                "label": "Archive",
                "autoDismissable": true
            }
        ],
        "schedule": {
            "timeZone": "America/New_York",
            "hour": "10",
            "minute": "0",
            "second": "0",
            "millisecond": "0",
            "allowWhileIdle": true,
            "repeat": true
        }
    }
}
EndOfMessage
	https://fcm.googleapis.com/fcm/send
