# pushboy

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## TODO

Notify users when there are channel changes/additions.

# push service notes
gotchas:
* if you rename the app, you can still receive notifications to the old app
* if you recreate the app with different channels, the old channels can persist, somehow (it's probably deep in the Android/iOS code layer)
* for iOS audio files, name must start with 'res_' and use aiff file format

# Notes
* to fix some androoid studio issues:
  * close the project, close android studio
  * delete .idea directories
  * delete all .iml files
* to get sha256: keytool -list -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android
* for ios creation, make sure to delete all Configurations (Debug, Release, Profile) under Project > Runner > Info tab
* for macOS, you need to integrate with apple again:
  * https://firebase.flutter.dev/docs/messaging/apple-integration/
  
### local state

for local state, you only need to store local values. remote values can always be retrieved later, instead of struggling to stay synced

## Building for ios

### do not delete Podfile. Do delete workspacex

* pod deintegrate
* pod cache clean --all

### Firebase Messaging errors:

* cannot find firebase_messaging.h
* add to Podfile
target 'ImageNotification' do
  use_frameworks!
  pod 'Firebase/Messaging'
end

### Could not run... try selecting "Product > Run"

## url_launcher notes
* need config changes for android and iOS

## unirversal links
* see nginx configuration for serving .well-known files
* the value of the appID is the app's Prefix and the bundle id. The app's Prefix is often the team ID, but sometimes it is different.
* validation tool at: https://branch.io/resources/aasa-validator/#resultsbox

